$(".qu-progress__line").each(function(index) {
    let first = parseInt($(this).attr('data-first'));
    let second = parseInt($(this).attr('data-second'));

    var progress = (first / (first + second)) * 100;
    $(this).css('width', progress + '%' );
});
