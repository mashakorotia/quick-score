$('.qu-dropdown__btn').on('click', function () {
    $(this).parent().toggleClass('active');
});

$(document).mouseup(function (e){
    var div = $(".qu-dropdown");
    if (!div.is(e.target) && div.has(e.target).length === 0) {
        $('.qu-dropdown.active').removeClass('active');
    }
});

$('.select2').select2({
    minimumResultsForSearch: -1,
});
