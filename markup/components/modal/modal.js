/*modal*/
$('a[data-modal], button[data-modal]').on('click', function () {
    let idModal = $(this).attr('data-modal');
    $(idModal).addClass('show');
});

$(document).mouseup(function (e){
    let modal = $(".qu-modal__content");
    if (!modal.is(e.target) && modal.has(e.target).length === 0) {
        $('.qu-modal.show').removeClass('show');
    }
});

