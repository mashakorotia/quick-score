$('a[data-tab]').on('click', function () {
    $('.qu-tabs__item.active').removeClass('active');
    $('a[data-tab].active').removeClass('active');

    let idTab = $(this).attr('data-tab');
    $(this).addClass('active');
    $(idTab).addClass('active');
});
