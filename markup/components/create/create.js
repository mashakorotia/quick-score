$(".qu-form-photo .js-file").jfilestyle({
    text: 'Choose File',
    input: false,
    'onChange': function (files) {
        console.log(files);
        $('.qu-form-photo__name').text(files[0].name);

    }
});

function readURL(e) {
    console.log(this.files[0]);
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.upload-img').attr('src', e.target.result);
            $('.upload-img').css({ display: "block" });
        };
        reader.readAsDataURL(this.files[0]);
    }
}

$(".js-file").change(readURL);

